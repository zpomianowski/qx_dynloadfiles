/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("dynloadfiles.demo.theme.Theme",
{
  meta :
  {
    color : dynloadfiles.demo.theme.Color,
    decoration : dynloadfiles.demo.theme.Decoration,
    font : dynloadfiles.demo.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : dynloadfiles.demo.theme.Appearance
  }
});