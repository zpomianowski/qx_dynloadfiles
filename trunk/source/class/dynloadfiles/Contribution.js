/* ************************************************************************

   Copyright:
     2009 ACME Corporation -or- Your Name, http://www.example.com

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Zbigniew Pomianowski

************************************************************************ */

/**
 * This is the main class of contribution "dynloadfiles"
 *
 * @asset(dynloadfiles/*)
 */
qx.Class.define("dynloadfiles.Contribution",
{
    extend : qx.ui.core.Widget,



    /*
    *****************************************************************************
     CONSTRUCTOR
    *****************************************************************************
    */

    /**
    * Create a new widget that load external js/css files
    *
    * @param codeArr {Array} Scripts to load
    * @param cssArr {Array} CSS files to load
    * @param base_path {String?null}
    */
    construct : function(codeArr, cssArr, base_path)
    {
        this.__path = base_path || qx.util.ResourceManager.getInstance().toUri("../static/");
        this.base(arguments);
        this.__addCssArr(cssArr);
        this.__loadScriptArr(codeArr, qx.lang.Function.bind(this.__loadElement, this));
    },

    statics : {
        INSTANCE_COUNTER : 0,
        LOADED: {},
        LOADING: {}
    },

    events : {
        scriptLoaded: 'qx.event.type.Event'
    },

    properties:
    {
        isLoaded:
        {
            nullable: true,
            init: false,
            apply: "__isLoaded"
        }
    },

    members : {
        __path: null,

        __loadScriptArr: function(codeArr, handler) {
            var that = this;
            var script = codeArr.shift();
            if (script){
                if (dynloadfiles.Contribution.LOADING[script]){
                    dynloadfiles.Contribution.LOADING[script].addListenerOnce('scriptLoaded',function(){
                        this.__loadScriptArr(codeArr,handler);
                    },this);
                }
                else if (dynloadfiles.Contribution.LOADED[script]){
                     this.__loadScriptArr(codeArr,handler);
                }
                else {
                    dynloadfiles.Contribution.LOADING[script] = this;
                    var sl = new qx.bom.request.Script();
                    var src = this.__path + script;
                    sl.onload = function() {
                        that.debug("Dynamically loaded: " + src);
                        that.__loadScriptArr(codeArr,handler);
                        dynloadfiles.Contribution.LOADED[script] = true;
                        dynloadfiles.Contribution.LOADING[script] = null;
                        that.fireDataEvent('scriptLoaded',script);
                    }
                    sl.open('GET', src);
                    sl.send();
                }
            } else {
                handler();
            }
        },

        __addCssArr: function(cssArr) {
            for (var i in cssArr) {
                var url = cssArr[i];
                if (!dynloadfiles.Contribution.LOADED[url]){
                    dynloadfiles.Contribution.LOADED[url]=true;
                    var head = document.getElementsByTagName("head")[0];
                    var el = document.createElement("link");
                    el.type = "text/css";
                    el.rel ="stylesheet";
                    el.href = this.__path + url;
                    setTimeout(function() {
                        head.appendChild(el);
                    }, 0);
                };
            }
        },

        __loadElement: function() {
            var el = this.getContentElement().getDomElement();
            if (el == null){
                this.addListenerOnce('appear', qx.lang.Function.bind(this.__loadElement, this), this);
            } else {
                dynloadfiles.Contribution.INSTANCE_COUNTER++;
                var el = this.getContentElement().getDomElement();
                this._onElementLoaded(el);
                this.addListener("disappear", this._destroy);
            }
        },

        _onElementLoaded: function(el) {
            console.log("_onElementLoaded not implemented");
        },

        _destroy: function() {
            console.log("_destroy not implemented");
        }
    }
});
