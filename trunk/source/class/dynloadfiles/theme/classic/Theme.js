/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("dynloadfiles.theme.classic.Theme",
{
  meta :
  {
    color : dynloadfiles.theme.classic.Color,
    decoration : dynloadfiles.theme.classic.Decoration,
    font : dynloadfiles.theme.classic.Font,
    appearance : dynloadfiles.theme.classic.Appearance,
    icon : qx.theme.icon.Oxygen
  }
});