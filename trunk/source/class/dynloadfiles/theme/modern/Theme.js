/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("dynloadfiles.theme.modern.Theme",
{
  meta :
  {
    color : dynloadfiles.theme.modern.Color,
    decoration : dynloadfiles.theme.modern.Decoration,
    font : dynloadfiles.theme.modern.Font,
    appearance : dynloadfiles.theme.modern.Appearance,
    icon : qx.theme.icon.Tango
  }
});